#pragma once

#include <cstddef>
namespace rd
{
double rand(double max = 1);
void init();
size_t uid();
}
